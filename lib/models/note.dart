class Note {
  int? _id;
  String? _title;
  String? _content;
  String? _dateCreated;
  String? _dateLastEdited;

  Note.update(this._id, this._title, this._content, this._dateCreated, this._dateLastEdited);

  Note(this._title, this._content, this._dateCreated, this._dateLastEdited);

  Note.map(dynamic obj) {
    _id = obj['id'];
    _title = obj['title'];
    _content = obj['content'];
    _dateCreated = obj['date_created'];
    _dateLastEdited = obj['date_last_edited'];
  }

  Note.fromMap(Map<String, dynamic> map) {
    _id = map['id'];
    _title = map['title'];
    _content = map['content'];
    _dateCreated = map['date_created'];
    _dateLastEdited = map['date_last_edited'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = new Map<String, dynamic>();

    map['title'] = _title;
    map['content'] = _content;
    map['date_created'] = _dateCreated;
    map['date_last_edited'] = _dateLastEdited;

    if (_id != null) map['id'] = _id;

    return map;
  }

  get title => _title;
  set title(value) {
    _title = value;
  }

  get content => _content;
  set content(value) {
    _content = value;
  }

  get dateCreated => _dateCreated;
  set dateCreated(value) {
    _dateCreated = value;
  }

  get dateLastEdited => _dateLastEdited;
  set dateLastEdited(value) {
    _dateLastEdited = value;
  }

  int? get id => _id;
  set id(int? value) {
    _id = value;
  }
}