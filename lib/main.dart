import 'package:flutter/material.dart';
import 'package:nd_notes/ui/home_page.dart';

void main() {
  runApp(MyApp());
}

ThemeData getThemeData() {
  // return ThemeData.light(
  //   useMaterial3: true,
  // );

  return ThemeData.dark(
    useMaterial3: true,
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ndNotes',
      theme: getThemeData(),
      home: HomePage(),
    );
  }
}
