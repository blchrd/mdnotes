import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nd_notes/models/note.dart';

class NoteEditPage extends StatelessWidget {
  const NoteEditPage({super.key, required this.note});

  final Note note;

  @override
  Widget build(BuildContext context) {
    final titleController = TextEditingController(text: note.title);
    final contentController = TextEditingController(text: note.content);

    void surroundTextSelection(String left, String right) {
      final currentTextValue = contentController.value.text;
      final selection = contentController.selection;
      final middle = selection.textInside(currentTextValue);
      final newTextValue =
          '${selection.textBefore(currentTextValue)}$left$middle$right${selection.textAfter(currentTextValue)}';

      contentController.value = contentController.value.copyWith(
        text: newTextValue,
        selection: TextSelection.collapsed(
          offset: selection.baseOffset + left.length + middle.length,
        ),
      );
    }

    void addCharacterAtStartOfLine(String start) {
      final text = contentController.text;
      final selection = contentController.selection;
      final lineStart = text.lastIndexOf('\n', selection.start - 1) + 1;

      contentController.text = text.replaceRange(lineStart, lineStart, start);
      contentController.selection = TextSelection.fromPosition(TextPosition(
        offset: selection.start + 1,
      ));
    }

    return Scaffold(
        appBar: AppBar(
          title: const Text("mdNotes"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: ListView(
            children: [
              Column(
                children: [
                  TextField(
                    decoration: const InputDecoration(
                      hintText: "Title", //babel text
                    ),
                    controller: titleController,
                  ),
                  const SizedBox(height: 16.0),
                  Column(children: [
                    TextField(
                      decoration: const InputDecoration(
                        hintText: "Markdown Content",
                      ),
                      controller: contentController,
                      minLines: 5,
                      maxLines: null,
                    ),
                    ButtonBar(
                      alignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                            icon: const Icon(Icons.format_bold),
                            iconSize: (18.0),
                            onPressed: () {
                              surroundTextSelection('**', '**');
                            }),
                        IconButton(
                            icon: const Icon(Icons.format_italic),
                            iconSize: (18.0),
                            onPressed: () {
                              surroundTextSelection('*', '*');
                            }),
                        IconButton(
                            icon: const Icon(Icons.format_strikethrough),
                            iconSize: (18.0),
                            onPressed: () {
                              surroundTextSelection('~~', '~~');
                            }),
                        IconButton(
                          icon: const Icon(Icons.title),
                          iconSize: (18.0),
                          onPressed: () {
                            addCharacterAtStartOfLine('## ');
                          }),
                        IconButton(
                          icon: const Icon(Icons.code),
                          iconSize: (18.0),
                          onPressed: () {
                            surroundTextSelection('```\n', '\n```');
                          }
                        ),
                        IconButton(
                          icon: const Icon(Icons.link),
                          iconSize: (18.0),
                          onPressed: () {
                            surroundTextSelection('[','](https://)');
                          },
                        ),
                      ],
                    ),
                  ]),
                ],
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.save),
          onPressed: () {
            Note updatedNote = note;
            updatedNote.title = titleController.text;
            updatedNote.content = contentController.text;
            updatedNote.dateLastEdited =
                DateFormat('yyyy-MM-dd – kk:mm').format(DateTime.now());

            Navigator.of(context).pop(updatedNote);
          },
        ));
  }
}
