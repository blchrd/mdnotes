class Constants {
  static final String TABLE_NAME = "noteTable";
  static final String COLUM_ID = "id";
  static final String COLUM_TITLE = "title";
  static final String COLUM_CONTENT = "content";
  static final String COLUM_DATE_CREATED = "date_created";
  static final String COLUM_DATE_LAST_EDITED = "date_last_edited";
  static final int DB_VERSION = 1;
  static final String DBNAME = "notes.db";
}