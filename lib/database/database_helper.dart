import 'dart:async';
import 'dart:io';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:nd_notes/utils/constants.dart';
import 'package:nd_notes/models/note.dart';

class DatabaseHelper {
  static final DatabaseHelper instance = DatabaseHelper.make();

  factory DatabaseHelper() => instance;

  static Database? _database;

  DatabaseHelper.make();

  Future<Database?> get database async {
    if (_database != null) return _database;
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, Constants.DBNAME);
    var myDb =
        openDatabase(path, version: Constants.DB_VERSION, onCreate: _onCreate);

    return myDb;
  }

  void _onCreate(Database? db, int version) async {
    await db?.execute(
      "CREATE TABLE ${Constants.TABLE_NAME} "
          "(${Constants.COLUM_ID} INTEGER PRIMARY KEY,"
          "${Constants.COLUM_TITLE} TEXT,"
          "${Constants.COLUM_CONTENT} TEXT,"
          "${Constants.COLUM_DATE_CREATED} TEXT,"
          "${Constants.COLUM_DATE_LAST_EDITED} TEXT"
          ")"
    );
  }

  // Insert a note into the database
  Future<int?> insert(Note note) async {
    Database? db = await instance.database;
    return await db?.insert(Constants.TABLE_NAME, note.toMap());
  }

  // Retrieve all notes from the database
  Future<List<Note>> getAllNotes() async {
    Database? db = await instance.database;
    List<Map<String, Object?>>? maps = await db?.query(Constants.TABLE_NAME);
    return List.generate(maps!.length, (i) {
      return Note.fromMap(maps[i]);
    });
  }

  // Update a note in the database
  Future<int?> update(Note note) async {
    Database? db = await instance.database;
    return await db?.update(Constants.TABLE_NAME, note.toMap(),
        where: '${Constants.COLUM_ID} = ?', whereArgs: [note.id]);
  }

  // Delete a note from the database
  Future<int?> delete(int id) async {
    Database? db = await instance.database;
    return await db?.delete(Constants.TABLE_NAME, where: '${Constants.COLUM_ID} = ?', whereArgs: [id]);
  }
}
