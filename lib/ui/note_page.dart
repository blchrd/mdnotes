import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:nd_notes/models/note.dart';

class NotePage extends StatelessWidget {
  const NotePage({super.key, required this.note});

  final Note note;

  @override
  Widget build(BuildContext context) {
    final title = note.title;
    final content = note.content;

    return Scaffold(
      appBar: AppBar(
        title: Text("mdNotes: " + title),
      ),
      body: Markdown(
        data: content,
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.edit),
        onPressed: () {
          Navigator.of(context).pop(note);
        },
      ),
    );
  }
}
