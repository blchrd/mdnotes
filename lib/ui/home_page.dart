import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nd_notes/database/database_helper.dart';
import 'package:nd_notes/models/note.dart';
import 'package:nd_notes/ui/note_edit_page.dart';
import 'package:nd_notes/ui/note_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final dbHelper = DatabaseHelper.instance;
  List<Note>? _notes = [];

  @override
  void initState() {
    super.initState();
    _loadNotes();
  }

  void _loadNotes() async {
    List<Note>? notes = await dbHelper.getAllNotes();
    setState(() {
      _notes = notes;
    });
  }

  void _addNote() async {
    Note newNote = Note(
        '', '', DateFormat('yyyy-MM-dd – kk:mm').format(DateTime.now()), '');

    Note? toAddNote = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => NoteEditPage(
          note: newNote,
        ),
      ),
    );

    if (toAddNote != null) {
      int? id = await dbHelper.insert(toAddNote);
      setState(() {
        toAddNote.id = id;
        _notes?.add(toAddNote);
      });
    }
  }

  void _showNote(int index) async {
    Note? update = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => NotePage(
          note: _notes![index],
        ),
      ),
    );

    if (update != null) {
      _updateNote(index);
    }
  }

  void _updateNote(int index) async {
    Note? updatedNote = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => NoteEditPage(
          note: _notes![index],
        ),
      ),
    );

    if (updatedNote != null) {
      await dbHelper.update(updatedNote);
      setState(() {
        _notes?[index] = updatedNote;
      });

      _showNote(index);
    }
  }

  void _deleteNote(int index) async {
    showDialog(
        context: context,
        builder: (BuildContext ctx) {
          return AlertDialog(
            title: const Text('Deletion'),
            content: const Text('Are you sure to delete this note?'),
            actions: [
              // The "Yes" button
              TextButton(
                  onPressed: () async {
                    await dbHelper.delete(_notes![index].id!);
                    setState(() {
                      _notes?.removeAt(index);
                    });

                    // Close the dialog
                    Navigator.of(context).pop();
                  },
                  child: const Text('Yes')),
              TextButton(
                  onPressed: () {
                    // Close the dialog
                    Navigator.of(context).pop();
                  },
                  child: const Text('No'))
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("mdNotes"),
        ),
        body: ListView.builder(
            itemCount: _notes?.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text(_notes?[index].title),
                subtitle: Text("Edited the ${_notes?[index].dateLastEdited}"),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    IconButton(
                      icon: const Icon(Icons.remove_red_eye),
                      onPressed: () {
                        _showNote(index);
                        // _updateNote(index);
                      },
                    ),
                    IconButton(
                        icon: const Icon(Icons.delete),
                        onPressed: () {
                          _deleteNote(index);
                        })
                  ],
                ),
              );
            }),
        floatingActionButton: FloatingActionButton(
            child: const Icon(Icons.add),
            onPressed: () {
              _addNote();
            }));
  }
}
